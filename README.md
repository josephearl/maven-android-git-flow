# Maven Git Flow

A script that integrates the Git Flow workflow and the Maven build process to provide a robust release pipeline.

## Commands

    start-release -v <RELEASE VERSION> -d <NEW SNAPSHOT VERSION>

    finish-release -v <RELEASE VERSION>

If there is a merge conflict finishing the release, resolve it (no need to commit) and run

    finish-release