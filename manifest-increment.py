#!/usr/bin/python3
import argparse
import fnmatch
import os
import re
import signal
import sys


def parseArgs():
    parser = argparse.ArgumentParser(
        description='Search for any AndroidManifest files and increment their version codes.')
    parser.add_argument('directory', nargs='?',
                        help='Directory to search for manifest files in.')
    parser.add_argument('--version', type=int, help='Specify a specific version number to set.')
    return parser.parse_args()


def locate(pattern, root=os.curdir):
    """Locate all files matching supplied filename pattern in and below
    supplied root directory."""
    for path, dirs, files in os.walk(os.path.abspath(root)):
        for filename in fnmatch.filter(files, pattern):
            yield os.path.join(path, filename)


def getFileList(directory):
    """ Returns a list of AndroidManifest.xml files in a given directory """
    print("Searching for AndroidManifest files...")
    if directory is None:
        files = locate("*AndroidManifest.xml")
    else:
        files = locate("*AndroidManifest.xml", directory)
    print("Finished...\n")
    return files


def processFile(file, version):
    # Read in the manifest file.
    manifestFile = open(file, "r")
    manifestContents = manifestFile.read()

    # Find the version code.
    matches = re.search('android:versionCode=\"(\d+)\"', manifestContents)
    oldVersion = int(matches.group(1))
    if version is None:
        newVersion = oldVersion + 1
        print("Incrementing version " + str(oldVersion) + " to " + str(newVersion) + ".\n")
    else:
        newVersion = version
        print("Setting version from " + str(oldVersion) + " to " + str(newVersion) + ".\n")

    newContents = re.sub('android:versionCode=\"(\d+)\"', 'android:versionCode="'+str(newVersion)+'"', manifestContents)

    # Write it out
    manifestFileWrite = open(file, "w")
    manifestFileWrite.write(newContents)


def printPrettyValidFiles(files):
    print("Found the following files:")
    for f in files:
        print("\t" + f)
    if len(files) == 0:
        print("\tNone found.")
    print("")


def signal_handler(signal, frame):
    """ Handle Ctrl-C exits. """
    print('\n\nYou pressed Ctrl+C! Exiting...')
    sys.exit(2)


def main(directory, version):
    files = getFileList(directory)
    validFiles = []
    for file in files:
        matches = re.search("src/main/AndroidManifest.xml", os.path.abspath(file))
        if matches:
            validFiles.append(file)
        else:
            matches = re.search("src/test/AndroidManifest.xml", os.path.abspath(file))
            if matches:
                validFiles.append(file)

    printPrettyValidFiles(validFiles)
    for validFile in validFiles:
        decisionInput = input("Process file: " + validFile + "? [y/N]: ")
        if decisionInput == 'y' or decisionInput == 'Y':
            processFile(validFile, version)
        else:
            print("Aborted.\n")
    print("All done!")

if __name__ == "__main__":
    if sys.version_info[0] < 3:
        print("Must be using Python 3")
        sys.exit(1)

    signal.signal(signal.SIGINT, signal_handler)
    # Parse the arguments and send them onto the main method.
    args = parseArgs()
    main(args.directory, args.version)